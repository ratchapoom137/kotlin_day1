abstract class Person(var name: String, var surname: String, var gpa: Double) {
    constructor(name: String, surname: String) : this(name, surname, 0.0) {

    }

    abstract fun goodBoy(): Boolean
    open fun getDetails(): String {
        return "$name $surname has score $gpa"
    }
}

class Student(name: String, surname: String, gpa: Double, var department: String) : Person(name, surname, gpa) {
    override fun goodBoy(): Boolean {
       return gpa > 2.0
    }

    override fun getDetails(): String {
        return "$name $surname has score $gpa and study in $department"
    }
}

fun main(args: Array<String>) {
    val no1: Person = Student("Sonchai", "Pitak", 2.00, "CMU")
    val no2: Person = Student("Prayu", "Son", 3.00, "CMU")
    val no3: Person = Student(surname = "Prayu", name = "Son", gpa = 4.00, department = "CMU")
    println(no1.getDetails())
    println(no2.getDetails())
    println(no3.getDetails())
}