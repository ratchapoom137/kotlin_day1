fun main(args: Array<String>) {
    showName("Ratchapoom", "Rattanakit")
    gradeReport()
    gradeReport("Somchai", 3.33)
    gradeReport(name = "Nobita")
    gradeReport(gpa = 2.50)
    println(isOdd(10))
    println(getAbbreviation('A'))
    println(arr(81))

    for (i in 6 downTo 0 step 2) {
        println(i)
    }

    var arrays = arrayOf(5, 55, 200, 1, 3, 5, 7)
    for ((index, value) in arrays.withIndex()) {
        println("$index. value = $value")
    }
    var max = findMaxValue(arrays)
    println("Max: $max")
}

fun showName(name: String, surname: String): Unit {
    println("name: $name $surname")
}

fun gradeReport(name: String = "annonymous", gpa: Double = 0.00): Unit = println("mister $name gpa: is $gpa")

fun isOdd(value: Int): String {
    if (value.rem(2) == 0) {
        return "$value is even value"
    } else {
        return "$value is odd value"
    }
}

fun getAbbreviation(abbr: Char): String {
    when (abbr) {
        'A' -> return "Abnormal"
        'B' -> return "Bad boy"
        'F' -> return "Fantastic"
        'C' -> {
            println("not smart")
            return "Cheap"
        }
        else -> return "Hello"
    }
}

fun getGrade(score: Int): String? {
    var grade: String?
    when (score) {
        in 0..50 -> grade = "F"
        in 51..70 -> grade = "C"
        in 71..80 -> grade = "B"
        in 81..100 -> grade = "A"
        else -> grade = null
    }
    return grade
}

fun arr(score: Int): String {
    var grade = getGrade(score)
    var result: String?
    if (grade == null) {
        result = "null"
    } else {
        result = "Your score is $score " + getAbbreviation(grade.toCharArray()[0])
    }
    return result
}

fun findMaxValue(values: Array<Int>): Int {
    var max = values[0]
    for ((index, value) in values.withIndex()) {
        if( value >= max) {
            max = value
        }
    }
    return max
}
