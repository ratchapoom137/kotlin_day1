data class Teacher(var name: String, var courseName: String) {

}

fun main(args: Array<String>) {
    var teacher1 : Teacher = Teacher("Somsak", "Democratic")
    var teacher2 : Teacher = Teacher("Somsak", "Democratic")
    var teacher3 : Teacher = Teacher("Somsak", "Mathematics")
    println(teacher1.equals(teacher3))
    println(teacher2.courseName)
    println(teacher2.name)
    println(teacher2.component1())
    println(teacher2.component2())
    var (teacherName, teacherCourseName) = teacher3
    println(teacher3)
}