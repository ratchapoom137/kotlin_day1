abstract class Animal(var legs: Int, var food: String) {
    abstract fun sound(): String
}

class Dog(legs: Int, food: String, var name: String) : Animal(legs, food) {
    override fun sound(): String {
        return "Dog: $name $legs $food sound=Bok Bok"
    }
}

class Lion(legs: Int, food: String, var name: String) : Animal(legs, food) {
    override fun sound(): String {
        return "Lion: $name $legs $food sound=ei ei"
    }
}

fun main(args: Array<String>) {
    val no1: Animal = Dog(4, "dog food", "Milo")
    val no2: Animal = Lion(4, "lion food", "piko")
    println(no1.sound())
    println(no2.sound())
}